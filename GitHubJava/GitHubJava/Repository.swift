//
//  Repository.swift
//  GitHubJava
//
//  Created by Michael Lima on 04/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import Foundation

class Repository {
    
    var name : String
    var description : String
    var ownerLogin : String
    var ownerAvatarUrl : String
    var forks : Int
    var stars : Int
    
    init() {
        self.name = ""
        self.description = ""
        self.ownerLogin = ""
        self.ownerAvatarUrl = ""
        self.forks = 0
        self.stars = 0
    }
    
    init(name:String, description:String, ownerLogin:String, ownerAvatarUrl:String, forks:Int, stars:Int) {
        self.name = name
        self.description = description
        self.ownerLogin = ownerLogin
        self.ownerAvatarUrl = ownerAvatarUrl
        self.forks = forks
        self.stars = stars
    }
}
