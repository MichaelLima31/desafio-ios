//
//  Pull.swift
//  GitHubJava
//
//  Created by Michael Lima on 05/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import Foundation

class Pull {
    
    var title : String
    var date : Date
    var body : String
    var userLogin : String
    var userAvatarUrl : String
    var htmlUrl : String
    
    init(title:String, date:Date, body:String, userLogin:String, userAvatarUrl:String, htmlUrl:String) {
        self.title = title
        self.date = date
        self.body = body
        self.userLogin = userLogin
        self.userAvatarUrl = userAvatarUrl
        self.htmlUrl = htmlUrl
    }
}
