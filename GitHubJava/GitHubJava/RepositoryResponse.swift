//
//  RepositoryResponse.swift
//  GitHubJava
//
//  Created by Swift on 03/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import Foundation
import ObjectMapper

class RepositoryResponse : Mappable {
    var name: String?
    var description: String?
    var owner: OwnerResponse?
    var forks: Int?
    var stars: Int?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        description <- map["description"]
        owner <- map["owner"]
        forks <- map["forks_count"]
        stars <- map["stargazers_count"]
    }
}
