//
//  OwnerResponse.swift
//  GitHubJava
//
//  Created by Michael Lima on 04/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import Foundation
import ObjectMapper

class OwnerResponse : Mappable {
    var login: String?
    var avatarUrl: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        login <- map["login"]
        avatarUrl <- map["avatar_url"]
    }
}
