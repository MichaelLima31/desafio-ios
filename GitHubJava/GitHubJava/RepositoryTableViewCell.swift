//
//  RepositoryTableViewCell.swift
//  GitHubJava
//
//  Created by Swift on 03/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textViewDescription: UITextView!
    @IBOutlet weak var labelOwnerLogin: UILabel!
    @IBOutlet weak var imageViewOwnerAvatar: UIImageView!
    @IBOutlet weak var labelForks: UILabel!
    @IBOutlet weak var labelStars: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
