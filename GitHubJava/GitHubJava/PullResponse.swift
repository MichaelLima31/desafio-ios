//
//  PullResponse.swift
//  GitHubJava
//
//  Created by Michael Lima on 05/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import Foundation
import ObjectMapper

class PullResponse : Mappable {
    var title : String?
    var date : Date?
    var body : String?
    var user: UserResponse?
    var htmlUrl : String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        date <- map["created_at"]
        body <- map["body"]
        user <- map["user"]
        htmlUrl <- map["html_url"]
    }
}
