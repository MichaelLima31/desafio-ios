//
//  PullTableViewController.swift
//  GitHubJava
//
//  Created by Michael Lima on 05/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class PullTableViewController: UITableViewController {
    
    //MARK: Properties
    var repository : Repository = Repository()
    var pulls : [Pull] = []
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "PullCell", bundle: nil), forCellReuseIdentifier: "PullCell")
        
        self.activityIndicator.center = self.view.center
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(self.activityIndicator)
        
        self.getPulls()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    
    //MARK: Own methods
    func startLoading() {
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func getPulls() {
        let url = "https://api.github.com/repos/\(self.repository.ownerLogin)/\(self.repository.name)/pulls"
        
        self.startLoading()
        
        Alamofire.request(url).responseArray { (response: DataResponse<[PullResponse]>) in
            if let data = response.result.value {
                for p in data {
                    let pull = Pull(
                        title: p.title ?? "",
                        date: p.date ?? Date(),
                        body: p.body ?? "",
                        userLogin: p.user?.login ?? "",
                        userAvatarUrl: p.user?.avatarUrl ?? "",
                        htmlUrl: p.htmlUrl ?? ""
                    )
                        
                    self.pulls.append(pull)
                }
                self.tableView.reloadData()
                self.stopLoading()
            }
        }
    }

    
    // MARK: TableViewDataSource methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pulls.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullCell", for: indexPath) as! PullTableViewCell
        
        cell.labelTitle.text = self.pulls[indexPath.row].title
        cell.textViewBody.text = self.pulls[indexPath.row].body
        cell.labelUserLogin.text = self.pulls[indexPath.row].userLogin
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        cell.labelDate.text = dateFormatter.string(from: self.pulls[indexPath.row].date)
        
        let placeholderImage = UIImage(named: "user.png")!
        let url = self.pulls[indexPath.row].userAvatarUrl
        cell.imageViewUserAvatar.sd_setImage(with: URL(string: url), placeholderImage: placeholderImage, options: [.continueInBackground, .progressiveDownload])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pull = self.pulls[indexPath.row]
        let url = URL(string: pull.htmlUrl)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
