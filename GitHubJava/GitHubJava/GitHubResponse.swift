//
//  GitHubResponse.swift
//  GitHubJava
//
//  Created by Michael Lima on 04/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import Foundation
import ObjectMapper

class GitHubResponse : Mappable {
    var totalCount: Int?
    var repositories: [RepositoryResponse]?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        totalCount <- map["total_count"]
        repositories <- map["items"]
    }
}
