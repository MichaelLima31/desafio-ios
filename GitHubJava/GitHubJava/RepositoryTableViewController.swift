//
//  RepositoryTableViewController.swift
//  GitHubJava
//
//  Created by Swift on 03/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class RepositoryTableViewController: UITableViewController {

    //MARK: Properties
    var repositories : [Repository] = []
    var currentPage : Int = 0
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "RepositoryCell", bundle: nil), forCellReuseIdentifier: "RepositoryCell")
        
        self.activityIndicator.center = self.view.center
        self.activityIndicator.hidesWhenStopped = true
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(self.activityIndicator)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maxOffset - offset) <= 40 {
            self.currentPage += 1
            self.getRepositories(atPage: self.currentPage)
        }
    }

    
    //MARK: Own methods
    func startLoading() {
        self.activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoading() {
        self.activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func getRepositories(atPage: Int) {
        let url = "https://api.github.com/search/repositories"
        let parameters: Parameters = [
            "q": "language:Java",
            "sort": "stars",
            "page": atPage
        ]
        
        self.startLoading()
        
        Alamofire.request(url, parameters: parameters).responseObject { (response: DataResponse<GitHubResponse>) in
            
            if let data = response.result.value {
                
                if let repositories = data.repositories {
                    for r in repositories {
                
                        let repository = Repository(
                            name: r.name ?? "",
                            description: r.description ?? "",
                            ownerLogin: r.owner?.login ?? "",
                            ownerAvatarUrl: r.owner?.avatarUrl ?? "",
                            forks: r.forks ?? 0, stars: r.stars ?? 0
                        )
                
                        self.repositories.append(repository)
                    }
                }
                
                self.tableView.reloadData()
                self.stopLoading()
            }
        }
    }
    
    
    //MARK: TableViewDataSource methods
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as! RepositoryTableViewCell
        
        cell.labelName.text = self.repositories[indexPath.row].name
        cell.textViewDescription.text = self.repositories[indexPath.row].description
        cell.labelOwnerLogin.text = self.repositories[indexPath.row].ownerLogin
        cell.labelForks.text = String(self.repositories[indexPath.row].forks)
        cell.labelStars.text = String(self.repositories[indexPath.row].stars)
        
        let placeholderImage = UIImage(named: "user.png")!
        let url = self.repositories[indexPath.row].ownerAvatarUrl
        cell.imageViewOwnerAvatar.sd_setImage(with: URL(string: url), placeholderImage: placeholderImage, options: [.continueInBackground, .progressiveDownload])
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repository = self.repositories[indexPath.row]
        performSegue(withIdentifier: "PullSegue", sender: repository)
    }
    
    
    //MARK: Navigation methods
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let repository = sender as! Repository
        let pullTableViewController = segue.destination as! PullTableViewController
        pullTableViewController.repository = repository
    }
}
