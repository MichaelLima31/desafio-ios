//
//  PullTableViewCell.swift
//  GitHubJava
//
//  Created by Michael Lima on 05/03/17.
//  Copyright © 2017 Michael Lima. All rights reserved.
//

import UIKit

class PullTableViewCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var textViewBody: UITextView!
    @IBOutlet weak var imageViewUserAvatar: UIImageView!
    @IBOutlet weak var labelUserLogin: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
